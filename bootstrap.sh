#!/bin/bash

# Script to be executed on the host

. ./.env 

docker exec -it ${DOCKER_CONTAINER_ES} /bin/bash -c "cd /${DOCKER_VOLUME_PROJECT} && bash ./bootstrap-container.sh"

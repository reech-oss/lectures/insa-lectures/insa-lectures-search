# Reech INSA Lectures - Search Engines / Elasticsearch

## Prerequisites

### Docker

**Attention!** If you are running another operating system than **Ubuntu**, refer to the [installation guide](https://docs.docker.com/engine/install/) for the proper distribution.

1. [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/).  
  *Install using the [repository](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).*

    ```bash
    # Update the apt package index and install packages to allow apt to use a repository over HTTPS
    sudo apt-get update
    sudo apt-get install --yes ca-certificates curl gnupg lsb-release

    # Add Docker’s official GPG key
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

    # Use the following command to set up the repository
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    # Install Docker Engine
    sudo apt-get update
    sudo apt-get install --yes docker-ce docker-ce-cli containerd.io docker-compose-plugin

    # Verify that Docker Engine is installed correctly by running the hello-world image
    sudo docker run hello-world
    ```

    <details>
    <summary>Expected ouput</summary>

    ```raw
    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    2db29710123e: Pull complete 
    Digest: sha256:62af9efd515a25f84961b70f973a798d2eca956b1b2b026d0a4a63a3b0b6a3f2
    Status: Downloaded newer image for hello-world:latest

    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
    1. The Docker client contacted the Docker daemon.
    2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (amd64)
    3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
    4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
    $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
    https://hub.docker.com/

    For more examples and ideas, visit:
    https://docs.docker.com/get-started/
    ```

    </details>

2. [Post-installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/).  
  *Manage Docker as a non-root user.*

    ```bash
    # The group docker may already exist.
    sudo groupadd docker
    sudo usermod -aG docker $USER
    # Log out and log back so that group membership is re-evaluated.
    # Verify that you can run docker commands without sudo.
    docker run hello-world
    ```

    **Attention!** A reboot may be required to flush the settings.

3. Additional setup:  
  *Avoid error "Error saving credentials: error storing credentials - err: exec: "docker-credential-secretservice": executable file not found in $PATH, out: ``" when running `docker login`.*

    ```bash
    sudo apt-get install --yes golang-docker-credential-helpers
    ```

4. [Install Docker Compose](https://docs.docker.com/compose/install/):

    ```bash
    sudo apt-get install --yes docker-compose
    ```

5. Download the images:

```bash
ES_VERSION=7.17.5

docker pull docker.elastic.co/elasticsearch/elasticsearch:${ES_VERSION}
docker pull docker.elastic.co/kibana/kibana:${ES_VERSION}
```

## Environment Setup

1. Setup and start the containers:

    ```bash
    cd insa-lectures-search
    source .env

    docker-compose build
    docker-compose up -d
    ```

2. Check the containers:

    ```bash
    curl http://localhost:${ES_PORT}
    curl -I http://localhost:${KB_PORT}
    ```

3. Initialize the containers:

    ```bash
    bash ./bootstrap.sh
    ```

4. Navigate to <http://localhost:5602/app/dev_tools>.  
  `5602` is the value of the environment variable `KB_PORT` (declared in the file [.env](.env)).

5. Verify the presence of the index `tiktok` by executing the command `GET _cat/indices/tiktok?v`.

## Environment Start & Stop

To **start** the containers:

```bash
docker-compose up -d
```

To **stop** the containers:

```bash
docker-compose stop
```

## Hands-On Labs

1. [Standard search](I-standard-search/README.md).
2. [Inverted index](II-inverted-index/README.md).
3. [Elasticsearch - Basics](III-elasticsearch/1-basic/README.md).
4. [Elasticsearch - Advanced](III-elasticsearch/2-advanced/README.md).
5. [Elasticsearch - Exercises](III-elasticsearch/3-exercises/README.md).

## Environment Cleaning

**Attention!** This will remove all the containers, networks, images, and volumes

To clean everything:

```bash
docker-compose down --rmi all --volumes
```

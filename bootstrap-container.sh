#!/bin/bash

# Script to be executed in the Elasticsearch container (triggered from the host by bootstrap.sh)

. ./.env

cd /${DOCKER_VOLUME_PROJECT}/III-elasticsearch/2-advanced

curl --request PUT \
  --url http://localhost:9200/tiktok \
  --header 'content-type: application/json' \
  --data '@tiktok-schema.jsonc'

curl --request POST \
  --url 'http://localhost:9200/tiktok/_bulk?refresh=' \
  --header 'content-type: application/x-ndjson' \
  --data-binary '@tiktok-bulk.ndjson'

cd -

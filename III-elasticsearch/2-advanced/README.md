# Elasticsearch - Advanced

Run the `.http` scripts in order ("a-", "b-", etc.).

The [Visual Studio Code](https://code.visualstudio.com/) extension [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) can be used to execute the `.http` files from the IDE.

**Attention!**

- The Elasticsearch container must be **started**.
- The variable `@port` must have the **same value** as `ES_PORT` from the top-level `.env` file.

The index `tiktok` is created using an [explicit mapping](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/explicit-mapping.html), defined in [tiktok-schema.jsonc](tiktok-schema.jsonc).  
Review the key components of the [mapping](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/mapping.html):

- Fields.
- Sub-fields.
- [Join field](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/parent-join.html).
- [Data types](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/mapping-types.html) (`keyword`, `text`, `date`, `long`, `boolean`).
- [Analyzers](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/analysis-analyzers.html) and [tokenizers](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/analysis-tokenizers.html) used for the text analysis.

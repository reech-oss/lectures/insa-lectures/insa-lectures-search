# Elasticsearch - Basics

Run the `.http` scripts in order ("a-", "b-", etc.).

The [Visual Studio Code](https://code.visualstudio.com/) extension [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) can be used to execute the `.http` files from the IDE.

**Attention!**

- The Elasticsearch container must be **started**.
- The variable `@port` must have the **same value** as `ES_PORT` from the top-level `.env` file.

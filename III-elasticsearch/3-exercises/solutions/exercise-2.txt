GET books/_search
{
  "query": {
    "match": {
      "first_sentence": "spleen"
    }
  },
  "highlight": {
    "fields": {
      "first_sentence": {}
    }
  }
}

GET books/_search
{
  "query": {
    "match_phrase": {
      "first_sentence": "she promised us"
    }
  },
  "highlight": {
    "fields": {
      "first_sentence": {}
    }
  }
}

GET books/_search
{
  "query": {
    "range": {
      "downloads": {
        "gte": 150000
      }
    }
  }
}

GET books/_search
{
  "query": {
    "range": {
      "release": {
        "gte": "2000-01-01"
      }
    }
  }
}

GET books/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "release": {
              "gte": "2000-01-01"
            }
          }
        },
        {
          "range": {
            "downloads": {
              "gte": 150000
            }
          }
        }
      ]
    }
  }
}

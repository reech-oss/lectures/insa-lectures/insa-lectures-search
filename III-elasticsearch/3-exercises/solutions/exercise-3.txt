GET books/_search?size=0
{
  "aggs": {
    "min_downloads": {
      "min": {
        "field": "downloads"
      }
    },
    "max_downloads": {
      "max": {
        "field": "downloads"
      }
    },
    "avg_downloads": {
      "avg": {
        "field": "downloads"
      }
    }
  }
}

GET books/_search?size=0
{
  "aggs": {
    "stats_downloads": {
      "stats": {
        "field": "downloads"
      }
    }
  }
}

GET books/_search?size=0
{
  "aggs": {
    "yearly_release": {
      "date_histogram": {
        "field": "release",
        "calendar_interval": "year"
      }
    }
  }
}

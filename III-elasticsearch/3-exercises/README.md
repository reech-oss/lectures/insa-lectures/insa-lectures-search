# Elasticsearch - Exercises

## Exercise 1 - Index Creation

1. Create an **index** `books` using [dynamic mapping](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/dynamic-mapping.html).
2. Create the **documents** to ingest the content of the file [books.csv](./data/books.csv) [^1].
3. Show the **content** of the index.

Elasticsearch documentation:

- <https://www.elastic.co/guide/en/elasticsearch/reference/7.17/indices-create-index.html>.
- <https://www.elastic.co/guide/en/elasticsearch/reference/7.17/docs-index_.html>.

[^1]: Source: <https://www.gutenberg.org/browse/scores/top>

## Exercise 2 - Search

1. Find the books with the **word** `spleen`.
2. Find the books with the **sentence** `she promised us`.
3. Find the books having more than 150 000 **downloads**.
4. Find the books released **after** year 2000.
5. Find the books released **after** year 2000 having more than 150 000 **downloads**.

Elasticsearch documentation:

- <https://www.elastic.co/guide/en/elasticsearch/reference/7.17/query-dsl.html>.

## Exercise 3 - Aggregates

1. Get the **minimum**, **maximum** and **average** number of downloads.
2. Get the **number** of releases per **year**.

Elasticsearch documentation:

- <https://www.elastic.co/guide/en/elasticsearch/reference/7.17/search-aggregations.html>.
